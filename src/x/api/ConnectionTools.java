package x.api;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.request.impl.RedirectRequest;
import lilypad.client.connect.api.result.FutureResultListener;
import lilypad.client.connect.api.result.StatusCode;
import lilypad.client.connect.api.result.impl.RedirectResult;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import x.api.messages.PartyMessage;
import x.api.parties.Party;
import x.api.parties.PartyManager;

public class ConnectionTools {

	public static void changeServers(Player player, String server, Boolean bringParty){

		Party p = PartyManager.getPlayerParty(player);
		if(p != null){
			if(!p.getLeader().getName().equals(player.getName())){
				PartyMessage.sysPlayerMessage(player, "You cant tp without your team");
				return;
			}

			if(bringParty){
				for(String s : p.getMembers()){
					if(Bukkit.getPlayer(s) != null){

						Player toSend = Bukkit.getPlayer(s);
						redirectRequest(server, toSend);
					}
				}
			}else{
				redirectRequest(server, player);
			}

			PartyManager.parties.remove(p);

		}else{
			redirectRequest(server, player);
		}


	}
	protected static void redirectRequest(String server, final Player player) {
		try {
			Connect c = Backend.getConnect();
			c.request(new RedirectRequest(server, player.getName()))
			.registerListener(
					new FutureResultListener<RedirectResult>() {
						public void onResult(
								RedirectResult redirectResult) {
							if (redirectResult.getStatusCode() == StatusCode.SUCCESS) {	
								return;
							}
							player.sendMessage("Could not connect");
						}
					});

		} catch (Exception exception) {
			player.sendMessage("Could not connect");
		}
	}

}
