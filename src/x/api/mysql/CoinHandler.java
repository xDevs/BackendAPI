package x.api.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import x.api.Backend;

public class CoinHandler {
	
	public static void updateCoins(Player p, Integer amount){
		
		if(hasCoins(p)){
			try {
				PreparedStatement ps = Backend.con.prepareStatement("UPDATE `"+ Backend.DBName +"_coins` SET coins=? WHERE player_uuid=?");
				ps.setInt(1, amount);
				ps.setString(2, p.getUniqueId().toString());
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}else{
			createCoins(p, amount);
		}
	}
	
	public static Integer getCoins(Player p){
		try {
			PreparedStatement ps = Backend.con.prepareStatement("SELECT * FROM " + Backend.DBName + "_coins WHERE player_uuid=?");
			ps.setString(1, p.getUniqueId().toString());
			
			ResultSet rs = ps.executeQuery();
			if(rs.isBeforeFirst()){
				while(rs.next()){
					return rs.getInt("coins");
				}
			}

			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	

		return 0;
	}
	
	public static Boolean hasCoins(Player p){
		try {
			PreparedStatement ps = Backend.con.prepareStatement("SELECT * FROM " + Backend.DBName + "_coins WHERE player_uuid=?");
			ps.setString(1, p.getUniqueId().toString());

			ResultSet rs = ps.executeQuery();
			if(rs.isBeforeFirst()){				
				while(rs.next()){
					return true;
				}
			}
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return false;
	}
	
	public static void createCoins(Player p, Integer amount){
		if(!hasCoins(p)){
			try {
				PreparedStatement ps = Backend.con.prepareStatement("INSERT INTO `"+ Backend.DBName +"_coins` VALUES (?,?)");
				ps.setString(1, p.getUniqueId().toString());
				ps.setInt(2, amount);
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}



}
