package x.api.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


import x.api.Backend;
import x.api.parties.Party;
import x.api.parties.PartyManager;

public class PartyHandler {

	public static void joinCheck(Player p){
		final String toLoad = PartyHandler.loadParty(p.getName());
		if (toLoad != null) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(
					Backend.getPlugin(), new Runnable() {
						public void run() {

							String[] spl1 = toLoad.split(":");
							String leader = spl1[0];

							String members = spl1[1];

							String activeMembers = "";
							String[] spl = members.split(",");
							for (String s : spl) {

								if (Bukkit.getPlayer(s) != null) {
									if (Bukkit.getPlayer(s).isOnline()) {
										if (activeMembers == "") {
											activeMembers = s;
										} else {
											activeMembers = activeMembers
													+ "," + s;
										}
									}
								}
							}
							Party pt = new Party(leader, activeMembers);
							PartyHandler.removeParty(pt);
						}
					}, 80L);
		}
	}

	public static void saveParty(Party p) {
		try {
			PreparedStatement ps = Backend.con.prepareStatement("INSERT INTO `"

					+ Backend.DBName + "_parties` VALUES (?,?)");


			ps.setString(1, p.getLeader().getName());
			ps.setString(2, PartyManager.getStringMembers(p));

			ps.execute();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static String loadParty(String party) {
		try {
			PreparedStatement ps = Backend.con
					.prepareStatement("SELECT * FROM " + Backend.DBName
							+ "_parties WHERE party_leader=?");

			ps.setString(1, party);

			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				while (rs.next()) {
					return rs.getString("party_leader")+":"+
							rs.getString("party_members");
				}
			}

			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void removeParty(Party p) {
		try {
			PreparedStatement ps = Backend.con.prepareStatement("DELETE FROM "
					+ Backend.DBName + "." + Backend.DBName
					+ "_parties WHERE party_leader=?");

			ps.setString(1, p.getLeader().getName());

			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
