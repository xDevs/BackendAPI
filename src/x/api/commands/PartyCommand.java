package x.api.commands;

import org.bukkit.entity.Player;

import x.api.messages.PartyMessage;
import x.api.parties.PartyManager;


public class PartyCommand {

	public PartyCommand(Player p, String[] args) {

		if (args.length == 2) {
			// Party Accept/Invite/remove

			if (args[0].equals("accept")) {

				PartyManager.join(p, args[1]);

			} else if (args[0].equals("invite")) {

				PartyManager.invite(args[1], p);
				
			} else if (args[0].equals("remove")) {

				PartyManager.kick(args[1], p);
			}else{
				
				PartyMessage.helpMessage(p);
				
			}
			
		} else if (args.length == 1) {
			// Party Leave

			if (args[0].equals("leave")) {

				PartyManager.leave(p);

			}else
			if(args[0].equals("help")){
				
				PartyMessage.helpMessage(p);
				
			}

		}else{
			
			PartyMessage.helpMessage(p);
			
		}

	}

}
