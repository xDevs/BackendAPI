package x.api.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import x.api.ConnectionTools;
import x.api.coins.Coins;

public class BackendExecutor implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {


		if(cmd.getName().equalsIgnoreCase("safestop")){
			new StopCommand();
		}

		if (sender instanceof Player) {

			Player p = (Player) sender;

			if (cmd.getName().equalsIgnoreCase("party")) {

				new PartyCommand(p, args);

			}else
				if (cmd.getName().equals("dev")) {

					p.sendMessage(""+Coins.getCoins(p));
					//Coins.setCoins(p, Coins.getCoins(p)+5);
					ConnectionTools.changeServers(p, "survival", true);
					
				}else
					if(cmd.getName().equalsIgnoreCase("hub")){

						ConnectionTools.changeServers(p, "hub", false);

					}
		}
		return false;
	}
}
