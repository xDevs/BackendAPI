package x.api.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import x.api.Backend;
import x.api.ConnectionTools;

public class StopCommand {

	public StopCommand() {
		
		for(Player p : Bukkit.getOnlinePlayers()){
			ConnectionTools.changeServers(p, "hub", false);
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Backend.getPlugin(), new Runnable(){
			public void run(){
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "stop");		
			}
		}, 20L * 4);
		
	}

}
