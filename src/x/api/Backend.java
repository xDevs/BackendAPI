package x.api;

import java.sql.Connection;
import java.sql.DriverManager;

import lilypad.client.connect.api.Connect;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import x.api.commands.BackendExecutor;
import x.api.events.CommandListener;
import x.api.events.PlayerChatListener;
import x.api.events.PlayerJoin;
import x.api.events.PlayerLeave;
import x.api.messages.StaffListener;
import x.api.messages.StaffMessage;
import x.api.ui.PartyUI;

public class Backend extends JavaPlugin {

	private static Backend plugin;

	// LilyPad
	private static Connect connect = null;

	// MySQL Connection
	public static Connection con;
	public static String DBName;

	public static String serverList = "hub,survival";
	public static StaffListener listener = new StaffListener();

	public void onEnable() {
		plugin = this;
		connect = (Connect) getServer().getServicesManager()
				.getRegistration(Connect.class).getProvider();

		connect.registerEvents(listener);

		DBName = "bukkit";
		createConnection("localhost", "xNetwork", "password", "3306", DBName);

		getCommand("party").setExecutor(new BackendExecutor());

		getCommand("dev").setExecutor(new BackendExecutor());
		getCommand("hub").setExecutor(new BackendExecutor());
		getCommand("safestop").setExecutor(new BackendExecutor());

		PartyUI.registerItems();

		Bukkit.getPluginManager()
				.registerEvents(new PlayerChatListener(), this);

		Bukkit.getPluginManager().registerEvents(new CommandListener(), this);
		Bukkit.getPluginManager().registerEvents(new StaffMessage(), this);

		Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerLeave(), this);
		Bukkit.getPluginManager().registerEvents(new PartyUI(), this);
	}

	public void onDisable() {

		connect.unregisterEvents(listener);
		connect = null;

		plugin = null;
	}

	public static Backend getPlugin() {
		return plugin;
	}

	public static Connect getConnect() {
		return connect;
	}

	protected Connection createConnection(String host, String user,
			String pass, String port, String dbName) {
		try {
			System.out.println("[BackendAPI] Connecting to server database!");
			Connection connection = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + dbName, user, pass);
			con = connection;
			System.out.println("[BackendAPI] Connected to mySQL!");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
