package x.api.ui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import x.api.parties.Party;
import x.api.parties.PartyManager;

public class PartyUI implements Listener {

	public static ItemStack partyGUI;

	public static void registerItems() {
		partyGUI = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ItemMeta partyMeta = partyGUI.getItemMeta();
		partyMeta.setDisplayName("�a�lParty �7(Right Click)");
		partyGUI.setItemMeta(partyMeta);

	}
// Are you able to still code?
	public Inventory playerList(Player p) {
		Party party = PartyManager.getPlayerParty(p);

		if (party == null) {
			party = new Party(p.getName(), "");
			party.addMember(p);
		}
		Inventory inv = Bukkit.createInventory(null, 9, "�8�lParty Size �f�l- " + "�9�l"+party.getMembers().size());
		
		for (int i = 0; i < party.getMembers().size(); i++) {

			SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(
					Material.SKULL_ITEM);
			meta.setOwner(party.getMembers().get(i));
			ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
			meta.setDisplayName("�9"+party.getMembers().get(i));
			item.setItemMeta(meta);

			inv.setItem(i, item);
		}

		return inv;
	}

	@EventHandler
	public void onPartyGUI(PlayerInteractEvent e) {
		if (e.getAction().name().contains("RIGHT")) {
			Player p = e.getPlayer();

			if (p.getItemInHand().equals(partyGUI)) {

				e.setCancelled(true);
				p.openInventory(playerList(p));

			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		e.setCancelled(true);
	}

}
