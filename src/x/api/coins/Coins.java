package x.api.coins;

import org.bukkit.entity.Player;

import x.api.mysql.CoinHandler;

public class Coins {

	public static Integer getCoins(Player p){
		return CoinHandler.getCoins(p);
	}
	
	public static void setCoins(Player p, Integer amount){
		CoinHandler.updateCoins(p, amount);
	}

}
