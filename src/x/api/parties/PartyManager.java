package x.api.parties;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import x.api.Backend;
import x.api.messages.PartyMessage;

public class PartyManager {

	public static List<Party> parties = new ArrayList<Party>();

	public static Party getParty(String owner) {

		for (Party party : parties) {
			if (party.getLeader().getName().equals(owner)) {
				return party;
			}
		}
		return null;
	}

	public static Party getPlayerParty(Player p) {

		for (Party party : parties) {
			if (party.getMembers().contains(p.getName())) {
				return party;
			}
		}
		return null;
	}

	public static String getStringMembers(Party p) {
		String list = "";
		for (String s : p.getMembers()) {
			if (list != "") {
				list = list + "," + s;
			} else {
				list = s;
			}
		}
		return list;
	}

	public static void sysPartyMessage(Party party, String msg) {
		for (String s : party.getMembers()) {
			if (Bukkit.getPlayer(s) != null) {
				if (Bukkit.getPlayer(s).isOnline()) {
					Bukkit.getPlayer(s).sendMessage(msg);
				}
			}
		}
	}

	public static void join(Player p, String name) {


		if (getParty(name) == null) {
			PartyMessage.plyChatMessage(p, "�aNo such party exists!");
			return;
		}

		if (getPlayerParty(p) != null) {
			if(getPlayerParty(p).getMembers().size() > 1){
				PartyMessage.plyChatMessage(p, "�aYou're already in a party!");

				return;
			}else{
				getPlayerParty(p).getMembers().clear();
				getPlayerParty(p).getPending().clear();
				parties.remove(getPlayerParty(p));
			}
		}


		Party party = getParty(name);

		
		if (party == null) {
			PartyMessage.plyChatMessage(p, "�aNo such party exists!");
			return;
		}
		


		if (!party.getPending().contains(p.getName())) {
			PartyMessage.plyChatMessage(p,
					"�aYou have not been invited to that party!");
			return;
		}
		PartyMessage.sysPartyMessage(party, "�a" + p.getName()
				+ " �ahas joined the party!");

		party.removePending(p.getName());
		party.addMember(p);
		PartyMessage.plyChatMessage(p, "�aYou have joined " + name
				+ "'s party!");

	}

	public static void kick(String toKick, Player kicker) {

		Party party = getPlayerParty(kicker);
		if (party == null) {
			PartyMessage.plyChatMessage(kicker, "�aYou're not in a party!");
			return;
		}

		Player p = Bukkit.getPlayer(toKick);
		if (p == null) {
			PartyMessage.plyChatMessage(kicker, "�aNo such player exists!");
			return;
		}
		if (!p.isOnline()) {
			PartyMessage.plyChatMessage(kicker, "�aNo such player exists!");
			return;
		}

		if (party.getMembers().contains(p.getName())) {

			party.removeMember(p);
			PartyMessage.sysPartyMessage(party, "�a" + p.getName()
					+ " has been kicked from your party!");
			PartyMessage.plyChatMessage(p, "�sYou have been kicked from "
					+ ChatColor.RED + kicker.getName() + "�4's party!");
		}

	}

	public static void leave(Player p) {

		Party party = getPlayerParty(p);
		if (party == null) {
			PartyMessage.plyChatMessage(p, "�aYou're not in a party!");
			return;
		}

		if (party.getLeader().getName().equals(p.getName())) {

			// Disband the party
			Iterator<String> it = party.getMembers().iterator();

			while (it.hasNext()) {
				String player = it.next();
				if (Bukkit.getPlayer(player).isOnline()) {
					PartyMessage.plyChatMessage(Bukkit.getPlayer(player),
							"�aYour party has been disbanded!");
					it.remove();
				}
			}

			party.getPending().clear();
			parties.remove(party);

		} else {

			// Just leave the party
			party.removeMember(p);
			PartyMessage.plyChatMessage(p,
					"�aYou have left your current party!");
			sysPartyMessage(party, ChatColor.GREEN + p.getName() + "�a has left the party!");

		}

	}

	public static void invite(String invited, Player inviter) {

		if (Bukkit.getPlayer(invited) == null) {
			PartyMessage.plyChatMessage(inviter, "�aNo such player exists!");
			return;
		}
		if (!Bukkit.getPlayer(invited).isOnline()) {
			PartyMessage.plyChatMessage(inviter, "�aNo such player exists!");
			return;
		}
		if (inviter.getName().equals(invited)) {
			PartyMessage.plyChatMessage(inviter,
					"�aYou cannot invite yourself to a party!");
			return;
		}

		final Player p = Bukkit.getPlayer(invited);

		Party party;

		if (getPlayerParty(inviter) != null) {
			if (!getPlayerParty(inviter).getLeader().getName()
					.equals(inviter.getName())) {
				PartyMessage.sysPlayerMessage(inviter,
						"�aYou are not the leader of this party!");
				return;
			}
		}

		if (getParty(inviter.getName()) == null) {
			party = new Party(inviter.getName(), "");
			party.addMember(inviter);
		} else {
			party = getParty(inviter.getName());
		}

		if (party.getPending().contains(p.getName())) {
			PartyMessage.sysPlayerMessage(inviter,
					"�aThat player already has an invitation to your party!");
			return;
		}

		
		if(party.getMembers().contains(p.getName())){
			PartyMessage.sysPlayerMessage(inviter, "�aThat player is already in the party!");
			return;
		}
		


		party.addPending(p.getName());

		PartyMessage.sysPlayerMessage(p, "�aYou've been invited to "
				+ party.getLeader().getName()
				+ "'s party. You can join using �a/party accept "
				+ party.getLeader().getName() + "�a. You have 15 seconds!");

		PartyMessage.sysPartyMessage(party, "�a" + p.getName()
				+ " has been invited to your party!");

		final String saved = inviter.getName();
		Bukkit.getScheduler().scheduleSyncDelayedTask(Backend.getPlugin(),
				new Runnable() {

			public void run() {

				Party pt = getParty(saved);
				if (pt == null) {
					return;
				}
				if (pt.getPending().contains(p.getName())) {
					if (!p.isOnline()) {
						return;
					}
					pt.removePending(p.getName());
					PartyMessage.sysPlayerMessage(p,
							"�aYour party invite from " + saved
							+ " has expired!");
					if (pt.getLeader() != null) {
						if (pt.getLeader().isOnline()) {
							PartyMessage.sysPlayerMessage(pt.getLeader(), ChatColor.GREEN + 
									p.getName()
									+ "'s invite has expired");
						}
					}
				}

			}

		}, 20L * 15);
	}

}
