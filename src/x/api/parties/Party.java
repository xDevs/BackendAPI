package x.api.parties;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Party {

	String leader;
	List<String> members;
	List<String> pending;

	public Party(String leader, String members) {

		this.leader = leader;

		this.members = new ArrayList<String>();

		if (members != "") {
			String[] spl = members.split(",");
			for (String s : spl) {
				this.members.add(s);
			}
		}

		this.pending = new ArrayList<String>();

		PartyManager.parties.add(this);

	}

	public Player getLeader() {
		return Bukkit.getPlayer(leader);
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	public List<String> getMembers() {
		return this.members;
	}

	public List<String> getPending() {
		return this.pending;
	}

	public void addPending(String s) {
		this.pending.add(s);
	}

	public void removePending(String s) {
		this.pending.remove(s);
	}

	public void addMember(Player p) {
		this.members.add(p.getName());
	}

	public void removeMember(Player p) {
		this.members.remove(p.getName());
	}
}
