package x.api.messages;

import java.io.UnsupportedEncodingException;

import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class StaffListener {
	
	@SuppressWarnings("unused")
	@EventListener
    public void onMessage(MessageEvent me) throws UnsupportedEncodingException {
        if (!me.getChannel().equals("staff")) {  //name of channel
            return;
        }
        String player;
        String message = "Error";
        String toSend = null;
        String prefix = "�4�lStaff �7�l> �r";

        try {
			String[] toSplit = me.getMessageAsString().split("~sender!");
			player = toSplit[1];
			toSend = prefix+"�7[�b"+player+"�7]�r "+toSplit[0];
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
        
        message = null;
		
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.hasPermission("x.staffchat")){
				p.sendMessage(toSend.replaceFirst("!", ""));
			}
		}
   }

}
