package x.api.messages;

import java.io.UnsupportedEncodingException;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.request.RequestException;
import lilypad.client.connect.api.request.impl.MessageRequest;
import lilypad.client.connect.api.result.FutureResult;
import lilypad.client.connect.api.result.impl.MessageResult;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import x.api.Backend;

@SuppressWarnings("deprecation")
public class StaffMessage implements Listener {
	
	@EventHandler
	public void onStaffChat(PlayerChatEvent e){
		Player p = e.getPlayer();
		if(e.getMessage().startsWith("!")){
			if(p.hasPermission("x.staffchat")){
				e.setCancelled(true);
				
				for(String servers : Backend.serverList.split(",")){
					request(servers, e.getMessage()+"~sender!"+p.getName());
				}
				
			}
		}
	}

	@SuppressWarnings("unused")
	public void request(String server, String message) {
        Connect c = Backend.getConnect();
        MessageRequest request = null;
        try {
            request = new MessageRequest(server, "staff", message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        FutureResult<MessageResult> futureResult = null;
        try {
            futureResult = c.request(request);
        } catch (RequestException e) {
            e.printStackTrace();
        }
	}

}
