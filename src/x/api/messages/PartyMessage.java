package x.api.messages;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import x.api.parties.Party;

public class PartyMessage {
	
	public static String getPrefix(){
		return "§c§lParty§7§l > ";
	}

	public static void sysPartyMessage(Party p, String message) {
		for (String s : p.getMembers()) {
			if (Bukkit.getPlayer(s) != null) {
				if (Bukkit.getPlayer(s).isOnline()) {
					Bukkit.getPlayer(s).sendMessage(getPrefix()+message);
				}
			}
		}
	}

	public static void sysPlayerMessage(Player p, String message) {
		p.sendMessage(getPrefix() + message);
		
	}

	public static void plyChatMessage(Player p, String message) {
		p.sendMessage(getPrefix() + message);
	}
	
	public static void helpMessage(Player p){
		p.sendMessage("§6-----------------------------------------------------");
		p.sendMessage("§aParty Commands:");
		p.sendMessage("§e/party help §b- Shows this help message");
		p.sendMessage("§e/party invite username §b- Invites the player to your party");
		p.sendMessage("§e/party leave §b- Leaves the current party");
		p.sendMessage("§e/party remove username §b- Remove the player from the party");
		p.sendMessage("§e/party accept username §b- Accept a party invite from the player");
		p.sendMessage("§6-----------------------------------------------------");
	}

}
