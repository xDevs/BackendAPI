package x.api.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onCommandPreProcess(PlayerCommandPreprocessEvent event) {
		if (event.getPlayer().isOp() == false) {
			if (event.getMessage().toLowerCase().startsWith("/plugins")
					|| event.getMessage().toLowerCase().startsWith("/pl")) {
				event.setCancelled(true);
				event.getPlayer().sendMessage("�fPlugins (2): �aLilyPad-Connect�f, �aBackendAPI");
			} else {
				event.setCancelled(false);
			}
		}
	}

}
