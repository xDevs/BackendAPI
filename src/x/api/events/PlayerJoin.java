package x.api.events;

import net.playerforcehd.nametags.TagManager.TagManager;
import net.playerforcehd.nametags.TagManager.TagPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import x.api.mysql.PartyHandler;
import x.api.ui.PartyUI;

public class PlayerJoin implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		p.getInventory().clear();
		p.getInventory().setItem(7, PartyUI.partyGUI);

		if (p.hasPermission("x.dev")) {
			TagManager.setPrefix(p, "&c&lDev &r");
		}
		if (p.hasPermission("x.manager")){
			TagManager.setPrefix(p, "&bManager &r");
		}
		if (p.hasPermission("x.hero")){
			TagManager.setPrefix(p, "&d&lHero &r");
		}
		if (p.hasPermission("x.vip")){
			TagManager.setPrefix(p, "&a&lVIP &r");
		}

		if (p.hasPlayedBefore()) {
			PartyHandler.joinCheck(p);
		}
	}
}
