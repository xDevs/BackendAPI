package x.api.events;

import net.playerforcehd.nametags.TagManager.TagManager;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeave implements Listener {

	@SuppressWarnings("deprecation")
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		TagManager.setPrefix(p, " ");
		TagManager.resetTag(p);
		TagManager.resetAll();

	}

}
